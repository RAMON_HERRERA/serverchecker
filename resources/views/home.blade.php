<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
            <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
        <div class="sidebar">
        <a href="#">Servidores</a>
        <a href="#">Revisión</a>
        <a href="#">Perfil</a>
    </div>

    <!-- Contenido principal -->
    <div class="container mt-5 ml-5">
        <h1>Contenido Principal</h1>
        <!-- Aquí puedes agregar el contenido principal de tu página -->
    </div>
</body>
</html>