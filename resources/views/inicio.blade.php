<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
            <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>

    <!--             ---------------------- Menú lateral --------------------- -->

    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-secondary">
                <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                    <a href="/" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline">Menu</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        <li class="nav-item">
                            <a href="#" class="nav-link align-middle px-0 text-white">
                                <i class="fs-4 bi-house"></i> <span class="ms-1 d-none d-sm-inline">Servidores</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-0 align-middle text-white">
                                <i class="fs-4 bi-table"></i> <span class="ms-1 d-none d-sm-inline"> Perfil</span></a>
                        </li>
                        <li>
                            <a href="#" class="nav-link px-0 align-middle text-white">
                                <i class="fs-4 bi-table"></i> <span class="ms-1 d-none d-sm-inline"> Registrar usuarios</span></a>
                        </li>
                    </ul>
                    <hr>
                </div>
            </div>

            <!-- Barra de opciones y buscador -->
            <div class="col">
                <div class="container mt-4">
                    <div class="row">
                        <!-- Campo de búsqueda -->
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Buscar">
                                <button class="btn btn-outline-secondary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <!-- Botón "Añadir" -->
                        <div class="col-md-2">
                            <button class="btn btn-success" onclick="añadirVm()">
                                <i class="fas fa-plus"></i> Añadir

                            </button>
                        </div>
                        <div class="col-md-2">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="tiempoDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Elegir Tiempo
                                </button>
                                <div class="dropdown-menu" aria-labelledby="tiempoDropdown">
                                    <a class="dropdown-item" href="#">5 minutos</a>
                                    <a class="dropdown-item" href="#">10 minutos</a>
                                    <a class="dropdown-item" href="#">15 minutos</a>
                                    <a class="dropdown-item" href="#">20 minutos</a>
                                    <a class="dropdown-item" href="#">30 minutos</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success" >
                                <i class="fas fa-plus"></i> Check now

                            </button>
                        </div>
                    </div>
                </div>


            <!-- Maquinas virtuales -->

                <div class="container mt-5">
                    <div class="card-columns">
                        <!-- Tarjeta 1 -->
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Nombre de la maquina</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">IP de Ejemplo: 192.168.1.100</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="card-text"> Status:
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-circle-fill " viewBox="0 0 16 16">
                                                <circle cx="8" cy="8" r="8"/>
                                            </svg> On
                                        </p>
                                    </div>
                                </div>           
                                <p class="card-text">Descripción: Lorem ipsum dolor sit amet, consectetur dipiscing elit.</p>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">Encendida desde hace 10 minutos</p>
                                    </div>
                                    <div class="col-6">
                                        <button class="btn btn-primary" onclick="mostrarAlerta()">Modificar</button>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <!-- Tarjeta 2 -->
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Nombre de la maquina</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">IP de Ejemplo: 192.168.1.100</p>
                                    </div>
                                    <div class="col-6">
                                        <p class="card-text"> Status:
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-circle-fill " viewBox="0 0 16 16">
                                                <circle cx="8" cy="8" r="8"/>
                                            </svg> On
                                        </p>
                                    </div>
                                </div>           
                                <p class="card-text">Descripción: Lorem ipsum dolor sit amet, consectetur dipiscing elit.</p>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">Encendida desde hace 10 minutos</p>
                                    </div>
                                    <div class="col-6">
                                        <button class="btn btn-primary" onclick="mostrarAlerta()">Modificar</button>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      

            <!-- Fin Maquinas virtuales -->

        </div>
    </div>
</body>
    
    <script>
        function mostrarAlerta() {
            Swal.fire({
                title: 'Editar VM (nombre vm)',
                html:
                    '<label for="ip">IP</label><br>' +
                    '<input type="text" id="ip" class="swal2-input"><br>' +
                    '<label for="nombre">Nombre</label><br>' +
                    '<input type="text" id="nombre" class="swal2-input"><br>' +
                    '<label for="descripcion">Descripción</label><br>' +
                    '<input type="text" id="descripcion" class="swal2-input">',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                focusConfirm: false,
                preConfirm: () => {
                    const ip = Swal.getPopup().querySelector('#ip').value;
                    const nombre = Swal.getPopup().querySelector('#nombre').value;
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value;
                    return { ip: ip, nombre: nombre, descripcion: descripcion };
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Datos ingresados:',
                        `IP: ${result.value.ip}, Nombre: ${result.value.nombre}, Descripción: ${result.value.descripcion}`,
                        'success'
                    );
                }
            });
        }

        function añadirVm() {
            Swal.fire({
                title: 'Añadir una nueva VM',
                html:
                    '<label for="ip">IP</label><br>' +
                    '<input type="text" id="ip" class="swal2-input"><br>' +
                    '<label for="nombre">Nombre</label><br>' +
                    '<input type="text" id="nombre" class="swal2-input"><br>' +
                    '<label for="descripcion">Descripción</label><br>' +
                    '<input type="text" id="descripcion" class="swal2-input">',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                focusConfirm: false,
                preConfirm: () => {
                    const ip = Swal.getPopup().querySelector('#ip').value;
                    const nombre = Swal.getPopup().querySelector('#nombre').value;
                    const descripcion = Swal.getPopup().querySelector('#descripcion').value;
                    return { ip: ip, nombre: nombre, descripcion: descripcion };
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Datos ingresados:',
                        `IP: ${result.value.ip}, Nombre: ${result.value.nombre}, Descripción: ${result.value.descripcion}`,
                        'success'
                    );
                }
            });
        }
    </script>
</html>

